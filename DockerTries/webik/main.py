
import uvicorn
from fastapi import FastAPI, Request, status
from fastapi import FastAPI, Request, status
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from os.path import isfile
from fastapi import Response
from mimetypes import guess_type

app = FastAPI()

@app.get("/templates/{filename}")
async def get_site(filename):
    filename = './templates/' + filename

    if not isfile(filename):
        return Response(status_code=404)

    with open(filename) as f:
        content = f.read()

    content_type, _ = guess_type(filename)
    return Response(content, media_type=content_type)


@app.get("/templates/")
async def get_site_default_filename():
    return await get_site('index.html')


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8080)
